//Kevin Echenique Arroyo #1441258

package movies.importer;

import java.io.*;
import java.util.ArrayList;

public class ProcessingTest {
	
	public static void main(String[] args) throws IOException{
			
		//Creates a lowecaseProcessor object with a source and a destination directory
		LowercaseProcessor processor = new LowercaseProcessor
				("C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Labs\\Lab5\\Lab_5", "C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Labs\\Lab5\\Lab_5\\NewBarFoo");
		
		/**
		 * notice that we are using a method from the Processor abstract class and since LowercaseProcessor
		 * class inherits it, we do not need to add that method (because it is not an abstract method)
		 * to the object class itself. So it implicitly inherited the execute() method.
		 */
		processor.execute();
		
		
		//Creates a RemoveDuplicates processor
		RemoveDuplicates processor2 = new RemoveDuplicates
				("C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Labs\\Lab5\\Lab_5", "C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Labs\\Lab5\\Lab_5\\NewBarFoo");
		processor2.execute();
		
		
		
		
		
		
		
		
		
		
		//FOR TESTING PURPOSES ONLY. PLEASE DISREGARD
		
		ArrayList<String> testArr = new ArrayList<String>();
		testArr.add("Hello");
		testArr.add("Hello2");
		testArr.add("Hello2");
		testArr.add("Hello3");
		testArr.add("Hello3");
		testArr.add("Hello4");
		//process(testArr);
		//testArr.remove(2);
		//boolean x = testArr.contains("Hello");
		//System.out.println(x);
		
	}
	
	//Testing the methods
/*public static void process(ArrayList<String> input) {
		
		//Creating an empty ArrayList
		ArrayList<String> asLower = new ArrayList<String>();
		
		for(int i=0; i<input.size(); i++) {
			//sets the String ArrayList at position i to be the same word, but all lowercase
			input.set(i, input.get(i).toLowerCase());
		}
		asLower = input;
		for(String current : asLower) {
		System.out.println(current);}
		//return asLower;
	}*/
	
/*public static void process(ArrayList<String> input){ //WORKING VERSION
		
		//this will store the values of input without the repeated values
				ArrayList<String> newInput = input;
				//first we iterate through every element if the given ArrayList and very whether or not it is repeated
				for(int i=0; i<input.size(); i++) {
					//After some thinking j=i+1, so j will never be at the the same position as i (because if they are at the same position they will obviously be equal and they will be removed even if they are singles and not duplicates)
					for(int j=i+1; j<input.size(); j++) {
						String currentWord = input.get(i);
						String iteratingWord = input.get(j);
						if(currentWord == iteratingWord) {
							newInput.remove(i);//Removes the repeated word from our new ArrayList
						}
					}	
				}
				//return newInput;
				for(String current : newInput) {
					System.out.println(current);
				}
			}*/
/*public static void process(ArrayList<String> input){
	
		String currentValue;
		for(int i=0; i < input.size(); i++) {
			//putting an array that only exists inside the loop and will reset its values after each iteration. This is used as a guide for the main array list
			ArrayList<String> temporalInput = input;
			currentValue = input.get(i);
			for(int j=0; j < temporalInput.size(); j++) {
				temporalInput.remove(i);//removes the word that we will be checking for duplicates
			}
			
			
		}

	}*/

//Another try
/*public static void countRepeated(ArrayList<String> input) {
	
	ArrayList<String> newInput;
	for(int i=0; i < input.size(); i++) {
		ArrayList<String> temporal = input;//Pseudo array because it does not exist outside the loop
		temporal.remove(i);//The idea is that the word that has already been compared will dissappear. we will compare array input to array temporal (the order matters) 
		for(int j=0; j < temporal.size(); j++) {
			if(input.get(i) == temporal.get(j)) {
				input.remove(i);//Removes the repeated value from the original array
				
			}	
		}
	}
	newInput = input;
	//return input;
	System.out.println("hello");
	
}*/
}
