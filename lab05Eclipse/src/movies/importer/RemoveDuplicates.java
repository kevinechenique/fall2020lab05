//Kevin Echenique Arroyo #1441258

package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor{
	
	public RemoveDuplicates(String sourceDir, String destDir){
			super(sourceDir, destDir, false);
	}
	
	//This method will remove any duplicates from the given ArrayList
	public ArrayList<String> process(ArrayList<String> input){
		
		//this will store the values of input without the repeated values
				ArrayList<String> newInput = input;
				//first we iterate through every element if the given ArrayList and very whether or not it is repeated
				for(int i=0; i<input.size(); i++) {
					//After some thinking j=i+1, so j will never be at the the same position as i (because if they are at the same position they will obviously be equal and they will be removed even if they are singles and not duplicates)
					for(int j=i+1; j<input.size(); j++) {
						String currentWord = input.get(i);
						String iteratingWord = input.get(j);
						if(currentWord == iteratingWord) {
							newInput.remove(i);//Removes the repeated word from our new ArrayList
						}
					}	
				}
				return newInput;
			}
}
