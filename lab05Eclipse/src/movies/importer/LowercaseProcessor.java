//Kevin Echenique Arroyo #1441258

package movies.importer;

import java.util.ArrayList;

public class LowercaseProcessor extends Processor{
	

	public LowercaseProcessor(String sourceDir, String destDir){
		super(sourceDir, destDir, true);	
	}

	//Method from the abstract class Processor
	public ArrayList<String> process(ArrayList<String> input) {
		
		//Creating an empty ArrayList
		ArrayList<String> asLower = new ArrayList<String>();
		
		for(int i=0; i<input.size(); i++) {
			//sets the String ArrayList at position i to be the same word, but all lowercase
			input.set(i, input.get(i).toLowerCase());
		}
		asLower = input;
		return asLower;
	}
	
	
}
